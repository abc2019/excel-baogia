const DECAL_GIAY = 1;
const DECAL_NHUA_TRANG = 2;
const DECAL_NHUA_TRONG = 3;
const DECAL_XI_BAC = 4;
const DECAL_KRAF = 5;
const DECAL_VO = 6;
const DECAL_7_MAU = 7;
const DECAL_VAI = 8;
const DECAL_BAO_MAT = 9;

const CAN_KHONG = 1;
const CAN_MO = 2;
const CAN_BONG = 3;

const BANG_GIA = {};
BANG_GIA[DECAL_GIAY] = {};
BANG_GIA[DECAL_GIAY][CAN_KHONG] = 30000;
BANG_GIA[DECAL_GIAY][CAN_MO] = 50000;
BANG_GIA[DECAL_GIAY][CAN_BONG] = 70000;

BANG_GIA[DECAL_NHUA_TRANG] = {};
BANG_GIA[DECAL_NHUA_TRANG][CAN_KHONG] = 30000;
BANG_GIA[DECAL_NHUA_TRANG][CAN_MO] = 50000;
BANG_GIA[DECAL_NHUA_TRANG][CAN_BONG] = 70000;

BANG_GIA[DECAL_NHUA_TRONG] = {};
BANG_GIA[DECAL_NHUA_TRONG][CAN_KHONG] = 30000;
BANG_GIA[DECAL_NHUA_TRONG][CAN_MO] = 50000;
BANG_GIA[DECAL_NHUA_TRONG][CAN_BONG] = 70000;

BANG_GIA[DECAL_XI_BAC] = {};
BANG_GIA[DECAL_XI_BAC][CAN_KHONG] = 30000;
BANG_GIA[DECAL_XI_BAC][CAN_MO] = 50000;
BANG_GIA[DECAL_XI_BAC][CAN_BONG] = 70000;

BANG_GIA[DECAL_KRAF] = {};
BANG_GIA[DECAL_KRAF][CAN_KHONG] = 30000;
BANG_GIA[DECAL_KRAF][CAN_MO] = 50000;
BANG_GIA[DECAL_KRAF][CAN_BONG] = 70000;

BANG_GIA[DECAL_VO] = {};
BANG_GIA[DECAL_VO][CAN_KHONG] = 30000;
BANG_GIA[DECAL_VO][CAN_MO] = 50000;
BANG_GIA[DECAL_VO][CAN_BONG] = 70000;

BANG_GIA[DECAL_7_MAU] = {};
BANG_GIA[DECAL_7_MAU][CAN_KHONG] = 30000;
BANG_GIA[DECAL_7_MAU][CAN_MO] = 50000;
BANG_GIA[DECAL_7_MAU][CAN_BONG] = 70000;

BANG_GIA[DECAL_VAI] = {};
BANG_GIA[DECAL_VAI][CAN_KHONG] = 30000;
BANG_GIA[DECAL_VAI][CAN_MO] = 50000;
BANG_GIA[DECAL_VAI][CAN_BONG] = 70000;

BANG_GIA[DECAL_BAO_MAT] = {};
BANG_GIA[DECAL_BAO_MAT][CAN_KHONG] = 30000;
BANG_GIA[DECAL_BAO_MAT][CAN_MO] = 50000;
BANG_GIA[DECAL_BAO_MAT][CAN_BONG] = 70000;

jQuery(document).ready(function () {


$("#calculate-form").on("afterValidateAttribute", function (event, fields, errors) {
    if ($("#decalpriceform-sizewidth").val() > 0
        && $("#decalpriceform-sizeheight").val() > 0
        && $("#decalpriceform-quantity").val() > 0
    ) {
        $("#order-btn").removeClass("hidden");
        var sizeWidth = $("#decalpriceform-sizewidth").val();
        var sizeHeight = $("#decalpriceform-sizeheight").val();
        var quantity = $("#decalpriceform-quantity").val();
        var decalType = $("#decalpriceform-decaltype").val();
        var decalTypeText = $("#decalpriceform-decaltype option:selected").text();
        var isExtrusion = $("#decalpriceform-isextrusion").val();
        var extrusionTypeText = $("#decalpriceform-isextrusion option:selected").text();
        var summarySize = sizeWidth + 'x' + sizeHeight + ' mm';
        // [REMOVED] var unitPrice = BANG_GIA[decalType][isExtrusion];
        // [REMOVED] var totalPrice = unitPrice * quantity;
        var totalPrice = tinhTongTien( sizeWidth, sizeHeight, quantity, decalType, isExtrusion ); // [ADDED]
        var unitPrice = tinhDonGia(totalPrice, quantity); // [ADDED]

        $("#unit_price").html(unitPrice.toLocaleString("vi-VN"));
        $("#total_order").html(totalPrice.toLocaleString("vi-VN"));
        $("#summary_size").html(summarySize);
        $("#summary_quantity").html(quantity.toLocaleString("vi-VN"));
        $("#summary_decal_type").html(decalTypeText + ", " + extrusionTypeText);

        // $("#for-sale-panel").removeClass("hidden");
        // $("#order-product").html(res.product_name);
        // $("#order-spec").html("Bình: "+res.techInfo.total_a+"x"+res.techInfo.total_b+ " con<br>Số tờ in: "+res.techInfo.total_page_print);
    }
});

$("#order-btn").on("click", function(){
    if ($("#decalpriceform-sizewidth").val() > 0
        && $("#decalpriceform-sizeheight").val() > 0
        && $("#decalpriceform-quantity").val() > 0
    ) {
        $.post("/cart/add", {
                sizeWidth: $("#decalpriceform-sizewidth").val(),
                sizeHeight: $("#decalpriceform-sizeheight").val(),
                quantity: $("#decalpriceform-quantity").val(),
                decalType: $("#decalpriceform-decaltype").val(),
                isExtrusion: $("#decalpriceform-isextrusion").val(),
                name: $("#summary_decal_type").html(),
                unit_price: $("#unit_price").html(),
                price: $("#total_order").html(),
                size: $("#summary_size").html()
            }, function (res) {
                if (res) {
                    window.location = "/cart";
                }
        });
    }
});

jQuery('#calculate-form').yiiActiveForm([{"id":"decalpriceform-sizewidth","name":"sizeWidth","container":".field-decalpriceform-sizewidth","input":"#decalpriceform-sizewidth","enableAjaxValidation":false,"validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Chiều ngang không được để trống."});yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Chiều ngang phải là số nguyên.","min":3,"tooSmall":"Chiều ngang không được nhỏ hơn 3","max":280,"tooBig":"Chiều ngang không được lớn hơn 280.","skipOnEmpty":1});}},{"id":"decalpriceform-sizeheight","name":"sizeHeight","container":".field-decalpriceform-sizeheight","input":"#decalpriceform-sizeheight","enableAjaxValidation":false,"validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Chiều cao không được để trống."});yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Chiều cao phải là số nguyên.","min":3,"tooSmall":"Chiều cao không được nhỏ hơn 3","max":300,"tooBig":"Chiều cao không được lớn hơn 300.","skipOnEmpty":1});}},{"id":"decalpriceform-quantity","name":"quantity","container":".field-decalpriceform-quantity","input":"#decalpriceform-quantity","enableAjaxValidation":false,"validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Số lượng decal không được để trống."});yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Số lượng decal phải là số nguyên.","min":1,"tooSmall":"Số lượng decal không được nhỏ hơn 1","skipOnEmpty":1});}},{"id":"decalpriceform-decaltype","name":"decalType","container":".field-decalpriceform-decaltype","input":"#decalpriceform-decaltype","enableAjaxValidation":false,"validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Loại decal không được để trống."});}},{"id":"decalpriceform-isextrusion","name":"isExtrusion","container":".field-decalpriceform-isextrusion","input":"#decalpriceform-isextrusion","enableAjaxValidation":false,"validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Có cán màng? không được để trống."});}}], []);
});
