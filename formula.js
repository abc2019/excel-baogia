function tinhDonGia(tongTien_str, soLuong_str) {
  var tongTien = parseInt(tongTien_str);
  var soLuong = parseInt(soLuong_str);
  return Math.round(tongTien / soLuong);
}

function tinhTongTien( sizeWidth_str, sizeHeight_str, quantity_str, decalType_str, isExtrusion_str ) {

  // [INITIAl] Convert string to integer
  const sizeWidth = parseInt(sizeWidth_str);
  const sizeHeight = parseInt(sizeHeight_str);
  const quantity = parseInt(quantity_str);
  const decalType = parseInt(decalType_str);
  const isExtrusion = parseInt(isExtrusion_str);

  // Chừa biên cắt (A13-A14)
  var chuaBienCat_Width = sizeWidth + 2;
  var chuaBienCat_Height = sizeHeight + 2;

  // Vùng in (A15-A16)
  const vungIn_Width = 280; // mm
  const vungIn_Height = 300; // mm

  //  Số con chiều A (B17-C17)
  var soConChieuA_1 = Math.floor(vungIn_Width/chuaBienCat_Width);
  // console.log(`vungIn_Width=${vungIn_Width};  chuaBienCat_Width=${chuaBienCat_Width}`);
  var soConChieuA_2 = Math.floor(vungIn_Height/chuaBienCat_Height);
  // console.log(`vungIn_Height=${vungIn_Height};  chuaBienCat_Height=${chuaBienCat_Height}`);

  //  Số con chiều B (B18-C18)
  var soConChieuB_1 = Math.floor(vungIn_Height/chuaBienCat_Width);
  var soConChieuB_2 = Math.floor(vungIn_Width/chuaBienCat_Height);
  // D17
  var soConChieuA = soConChieuA_1 * soConChieuA_2;
  // console.log(`soConChieuA_1=${soConChieuA_1};  soConChieuA_2=${soConChieuA_2}`);
  
  // D18
  var soConChieuB = soConChieuB_1 * soConChieuB_2;
  // console.log(`soConChieuB_1=${soConChieuB_1};  soConChieuB_2=${soConChieuB_2}`);

  // console.log(`soConChieuA=${soConChieuA};  soConChieuB=${soConChieuB}`);

  // D19
  var soConTren1To = (soConChieuB > soConChieuA) ? soConChieuB : soConChieuA;
  // D20
  var soTo = quantity / soConTren1To;
  // F5
  var soToIn = ( soTo < 1) ? 1 : soTo;
  // console.log(`soConTren1To=${soConTren1To};  soToIn=${soToIn}`);

  // // //  Chu vi mỗi con (A23)
  // // var chuViMoiCon = sizeWidth * 2 + sizeHeight * 2;
  // // //  Độ dài đường bế (B24)
  // // var doDaiDuongBe = chuViMoiCon * soConTren1To;

  // Tổng tiền cơ bản
  var luyTienThanhTien = bangTinhLuyTien(soToIn);
  var tongTienCoBan = Math.round(tinhTongTienCoBan(luyTienThanhTien));

  //  Tiền decal tính thêm 
  var tienDecalTinhThem = Math.round(tinhTienDecalThem(soToIn, decalType));

  //  Tiền cán màng 
  var tienCanMang = Math.round(tinhTienCanMang(soToIn, isExtrusion));

  // Tiền cắt tính thêm 
  var tienCatThem = Math.round(tinhTienCatTinhThem(soConTren1To, soToIn)); // TODO Trong Excel không cộng vào. Hiển thị ở đâu?

  // Tổng tiền
  // console.log(`tongTienCoBan=${tongTienCoBan}; tienDecalTinhThem=${tienDecalTinhThem}; tienCanMang=${tienCanMang}; tienCatThem=${tienCatThem}`);
  return tongTienCoBan + tienDecalTinhThem + tienCanMang;
}

/**
 * Tính tổng tiền cơ bản
 * @param {number} soToIn Số tờ in 
 */
function bangTinhLuyTien(soToIn) {

  const row = [
    {
      soLuong: 1,
      giaTien: 100000
    },
    {
      soLuong: 2,
      giaTien: 40000
    },
    {
      soLuong: 3,
      giaTien: 20000
    },
    {
      soLuong: 11,
      giaTien: 8000
    },
    {
      soLuong: 21,
      giaTien: 5000
    },
    {
      soLuong: 31,
      giaTien: 4500
    },
    {
      soLuong: 41,
      giaTien: 4200
    },
    {
      soLuong: 51,
      giaTien: 4000
    },
    {
      soLuong: 101,
      giaTien: 3800
    },
    {
      soLuong: 201,
      giaTien: 3600
    },
    {
      soLuong: 301,
      giaTien: 3400
    },
    {
      soLuong: 401,
      giaTien: 3200
    },
    {
      soLuong: 501,
      giaTien: 3000
    },
    {
      soLuong: 601,
      giaTien: 2800
    },
    {
      soLuong: 701,
      giaTien: 2600
    },
    {
      soLuong: 801,
      giaTien: 2400
    },
    {
      soLuong: 900,
      giaTien: 2300
    },
    {
      soLuong: 1000,
      giaTien: 2200
    }
  ];

  var thanhTien = [];

  for (var i = 0; i < row.length; i++) {
    var soTo = 0;
    switch (i) {
      case 0: // soLuong = 1
        soTo = (soToIn >= 1) ? 1 : 0;
        break;
      case 1: // soLuong = 2
        soTo = (soToIn > 2) ? 1 : 0;
        break;
      case 2: // soLuong = 3
        soTo = 0;
        if (soToIn >= 3) {
          if (soToIn <= 11) {
            soTo = soToIn - 3 + 1;
          } else {
            soTo = 8;
          }
        }
        break;
      case 3:
      case 4:
      case 5:
      case 6:
        soTo = 0;
        if (soToIn > row[i].soLuong) {
          if (soToIn <= row[i+1].soLuong) {
            soTo = soToIn - row[i].soLuong + 1;
          } else {
            soTo = 10;
          }
        }
        break;
      case 7:
        soTo = 0;
        if (soToIn > row[i].soLuong) {
          if (soToIn <= row[i+1].soLuong) {
            soTo = soToIn - row[i].soLuong + 1;
          } else {
            soTo = 50;
          }
        }
        break;
      case row.length-1: // trên 1000
        soTo = 0;
        if (soToIn > row[i].soLuong) {
          if (soToIn <= 1000000000) {
            soTo = soToIn - row[i].soLuong + 1;
          } else {
            soTo = 50;
          }
        }
        break;
      default:
        soTo = 0;
        if (soToIn > row[i].soLuong) {
          if (soToIn <= row[i+1].soLuong) {
            soTo = soToIn - row[i].soLuong + 1;
          } else {
            soTo = 100;
          }
        }
        break;
    }
    var tien = soTo * row[i].giaTien;
    // // console.log(`bangTinhLuyTien: i=${i}, soTo=${soTo}, tien=${tien}`);
    thanhTien.push( tien );
  }
  return thanhTien;
}

function getSum(total, num) {
  return total + num;
}

function tinhTongTienCoBan(arr) {
  return arr.reduce(getSum);
}

/**
 * Tiền decal tính thêm
 * @param {number} soToIn Số tờ in
 * @param {number} decalType Loại decal
 */
function tinhTienDecalThem(soToIn, decalType) {
  /*
<option value="1">Decal giấy</option>
<option value="2">Decal nhựa trắng</option>
<option value="3">Decal nhựa trong</option>
<option value="4">Decal xi bạc</option>
<option value="5">Decal kraf</option>
<option value="6">Decal vỡ (tem bảo hành)</option>
<option value="7">Decal 7 màu</option>
<option value="8">Decal vải</option>
<option value="9">Decal bảo mật chống gỡ</option>
  */

  var tienDecal;

  switch (decalType) {
    case 1: tienDecal = 0; break;
    case 2: tienDecal = 1200; break;
    case 3: tienDecal = 1200; break;
    case 4: tienDecal = 1200; break;
    case 6: tienDecal = 7500; break;
    default: tienDecal = 0; break;
  }

  return soToIn * tienDecal;
}

function tinhTienCanMang(soToIn, isExtrusion) {

/*
<option value="2">Cán màng mờ </option>
<option value="3">Cán màng bóng </option>
<option value="1" selected="">Không cán màng</option>
*/

  var tienCanMang = 0;
  switch (isExtrusion) {
    case 1: tienCanMang = 0; break;
    case 2: tienCanMang = 500; break;
    case 3: tienCanMang = 500; break;
    default: tienCanMang = 0; break;
  }

  return soToIn * tienCanMang;
}

function tinhTienCatTinhThem(soConTren1To, soToIn) {
  var chia200 = Math.ceil( soConTren1To / 200 ); // Math.ceil == ROUNDUP
  // console.log(`chia200=${chia200}`);
  var tien = 0;
  if (soConTren1To >= 200) {
    tien = 500 * chia200 * soToIn;
  }
  // console.log(`tien=${tien}`);

  return tien;
}